import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationImplService } from './authentication-impl.service';

describe('AuthenticationImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationImplService]
    });
  });

  it('should be created', inject([AuthenticationImplService], (service: AuthenticationImplService) => {
    expect(service).toBeTruthy();
  }));
});
