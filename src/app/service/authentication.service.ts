import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


export abstract class AuthenService {
    abstract login(username: string, password: string);
    abstract logout();
    abstract  getToken(): string;
    abstract getCurrentUser();
    abstract hasRole(role: string): boolean;
}
